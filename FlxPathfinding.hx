package flixel.tile;

	import flixel.FlxBasic;
	import flixel.math.FlxMath;
	import flixel.math.FlxPoint;
	import flixel.tile.FlxTilemap;
	import flixel.FlxG;
	/**
	 * Class which implements the A* pathfinding algorithm
	 * @author Vexhel
	 * Thanks to Rolpege and Valenti for fixes
	 */
	class FlxPathfinding {
		private var _map_WidthInTiles:Int;
		private var _map_HeightInTiles:Int;
		private var _nodes:Array<FlxPathfindingNode>;
		private var _open:Array<FlxPathfindingNode>;
		private var _closed:Array<FlxPathfindingNode>;
		
		//we use int (instead of Number) to get better performance
		static private var COST_ORTHOGONAL:Int = 10;
		static private var COST_DIAGONAL:Int = 14;
		static private var COST_DIG_WALL:Int = 1024;
		
		public var allowDiagonal:Bool = true;
		public var calculateNearestPoint:Bool = false;
		public function new(tilemap:FlxTilemap) {
			//initialize the node structure
			_map_WidthInTiles = tilemap.widthInTiles;
			_map_HeightInTiles = tilemap.heightInTiles;
			_nodes = new Array<FlxPathfindingNode>();
			for (i in 0...tilemap.totalTiles) {
				_nodes[i] = new FlxPathfindingNode(i % _map_WidthInTiles, Std.int(i / _map_WidthInTiles), (tilemap.getTileByIndex(i) < tilemap._collideIndex), 0);
			}
		}
		
		//given a start and an end points, returns an array of points representing the shortest path (if any) between them
		public function findPath(startPoint:FlxPoint, endPoint:FlxPoint, dig:Bool):Array<FlxPoint> {
			_open = new Array<FlxPathfindingNode>();
			_closed = new Array<FlxPathfindingNode>();
			for (i in 0..._nodes.length) {
				_nodes[i].parent = null;
			}
			var spX: Int = cast(startPoint.x, Int);
			var spY: Int = cast(startPoint.y, Int); 
			var epX: Int = cast(endPoint.x, Int);
			var epY: Int = cast(endPoint.y, Int);
			var startNode:FlxPathfindingNode = new FlxPathfindingNode(spX, spY, false, 0);
			startNode = getNodeAt(spX, spY);
			var endNode:FlxPathfindingNode = new FlxPathfindingNode(epX, epY, false, 0);
			endNode = getNodeAt(epX, epY);
			startNode.g = 0;
			startNode.h = calcDistance(startNode, endNode);
			startNode.f = startNode.h;	
			_open.push(startNode);

			while (_open.length > 0) {
				var f:Int = FlxMath.MAX_VALUE_INT;
				var currentNode:FlxPathfindingNode = new FlxPathfindingNode(0, 0, true, 0);
				//choose the node with the lesser cost f
				for (i in 0..._open.length) {
					if (_open[i].f < f) {
						currentNode = _open[i];
						f = currentNode.f;
					}
				}
				//we arrived at the end node, so we finish
				if (currentNode == endNode) {
					return rebuildPath(currentNode);
				}
				//we visited this node, so we can remove it from open and add it to closed
				_open.splice(_open.indexOf(currentNode), 1);
				_closed.push(currentNode);
				//do stuff with the neighbors of the current node
				for (n in getNeighbors(currentNode, dig)) {
					//skip nodes that has already been visited
					if (_closed.indexOf(n) > -1) {
						continue;
					}
					var g:Int = currentNode.g + n.cost;
					if (_open.indexOf(n) == -1) {
						_open.push(n);
						n.parent = currentNode;
						n.g = g;					//path travelled so far
						n.h = calcDistance(n, endNode); //estimated path to goal
						n.f = n.g + n.h;
					} else if (g < n.g) {
						n.parent = currentNode;
						n.g = g;
						n.h = calcDistance(n, endNode);
						n.f = n.g + n.h;
					}
				}
			}
			//no path can be found
			if(calculateNearestPoint)
			{
				var min:Int = FlxMath.MAX_VALUE_INT;
				var nearestNode:FlxPathfindingNode = new FlxPathfindingNode(0,0,true,0);
				//find the reachable node that is nearer to the goal
				for (c in _closed) {
					var dist:Int = calcDistance(c, endNode);
					if (dist < min) {
						min = dist;
						nearestNode = c;
					}
				}
				return rebuildPath(nearestNode); //returns the path to the node nearest to the goal
			}
			else
			{
				return [];
			}
		}
		
		
		public function getNodeAt(x:Int, y:Int):FlxPathfindingNode {
			return _nodes[x + (y * _map_WidthInTiles)];
		}
		
		//returns an array from a linked list of nodes
		private function rebuildPath(end:FlxPathfindingNode):Array<FlxPoint> {
			var path:Array<FlxPoint> = [];
			if (end == null) {
				return path;
			}
			var n:FlxPathfindingNode = end;
			while (n.parent != null) {
				path.push(new FlxPoint(n.x, n.y));
				n = n.parent;
			}
			path.reverse();
			return path;
		}
		
		private function getNeighbors(node:FlxPathfindingNode, dig:Bool):Array<FlxPathfindingNode> {
			var x:Int = node.x;
			var y:Int = node.y;
			var currentNode:FlxPathfindingNode = new FlxPathfindingNode(0,0,false,0);
			var neighbors:Array<FlxPathfindingNode> = [];
			var costModify:Int;
			costModify = currentNode.movecost;
			if (x > 1) {
				currentNode = getNodeAt(x - 1, y);
				if (currentNode.walkable) {
					currentNode.cost = COST_ORTHOGONAL + costModify;
					neighbors.push(currentNode);
				} else {
					if (dig) {
						currentNode.cost = COST_DIG_WALL + COST_ORTHOGONAL + costModify;
						neighbors.push(currentNode);
					}
				}
			}
			if (x < _map_WidthInTiles - 2) {
				currentNode = getNodeAt(x + 1, y);
				if (currentNode.walkable) {
					currentNode.cost = COST_ORTHOGONAL + costModify;
					neighbors.push(currentNode);
				} else {
					if (dig) {
						currentNode.cost = COST_DIG_WALL + COST_ORTHOGONAL + costModify;
						neighbors.push(currentNode);
					}
				}
			} 
			if (y > 1) {
				currentNode = getNodeAt(x, y - 1);
				if (currentNode.walkable) {
					currentNode.cost = COST_ORTHOGONAL + costModify;
					neighbors.push(currentNode);
				} else {
					if (dig) {
						currentNode.cost = COST_DIG_WALL + COST_ORTHOGONAL + costModify;
						neighbors.push(currentNode);
					}
				}
			}
			if (y < _map_HeightInTiles - 2) {
				currentNode = getNodeAt(x, y + 1);
				if (currentNode.walkable) {
					currentNode.cost = COST_ORTHOGONAL + costModify;
					neighbors.push(currentNode);
				} else {
					if (dig) {
						currentNode.cost = COST_DIG_WALL + COST_ORTHOGONAL + costModify;
						neighbors.push(currentNode);
					}
				}
			}
			if (allowDiagonal){
				if (x > 1 && y > 1) {
					currentNode = getNodeAt(x - 1, y - 1);
					if (currentNode.walkable && getNodeAt(x - 1, y).walkable && getNodeAt(x, y - 1).walkable) {
						currentNode.cost = COST_DIAGONAL + costModify;
						neighbors.push(currentNode);
					} else {
						if (dig) {
							currentNode.cost = COST_DIG_WALL + COST_DIAGONAL + costModify;
							neighbors.push(currentNode);
						}
					}
				}
				if (x < _map_WidthInTiles - 2 && y > 1) {
					currentNode = getNodeAt(x + 1, y - 1);
					if (currentNode.walkable && getNodeAt(x + 1, y).walkable && getNodeAt(x, y - 1).walkable) {
						currentNode.cost = COST_DIAGONAL + costModify;
						neighbors.push(currentNode);
					} else {
						if (dig) {
							currentNode.cost = COST_DIG_WALL + COST_DIAGONAL + costModify;
							neighbors.push(currentNode);
						}
					}
				}
				if (x > 1 && y < _map_HeightInTiles - 2) {
					currentNode = getNodeAt(x - 1, y + 1);
					if (currentNode.walkable && getNodeAt(x - 1, y).walkable && getNodeAt(x, y + 1).walkable) {
						currentNode.cost = COST_DIAGONAL + costModify;
						neighbors.push(currentNode);
					} else {
						if (dig) {
							currentNode.cost = COST_DIG_WALL + COST_DIAGONAL + costModify;
							neighbors.push(currentNode);
						}
					}
				}
				if (x < _map_WidthInTiles - 2 && y < _map_HeightInTiles - 2) {
					currentNode = getNodeAt(x + 1, y + 1);
					if (currentNode.walkable && getNodeAt(x + 1, y).walkable && getNodeAt(x, y + 1).walkable) {
						currentNode.cost = COST_DIAGONAL + costModify;
						neighbors.push(currentNode);
					} else {
						if (dig) {
							currentNode.cost = COST_DIG_WALL + COST_DIAGONAL + costModify;
							neighbors.push(currentNode);
						}
					}
				}
			}
			return neighbors;
		}
		
		//cheap way to calculate an approximate distance between two nodes
		private function calcDistance(start:FlxPathfindingNode, end:FlxPathfindingNode):Int {
			if (start.x > end.x) {
				if (start.y > end.y) {
					return (start.x - end.x) + (start.y - end.y);
				} else {
					return (start.x - end.x) + (end.y - start.y);
				}
			} else {
				if (start.y > end.y) {
					return (end.x - start.x) + (start.y - end.y);
				} else {
					return (end.x - start.x) + (end.y - start.y);
				}
			}
		}
		
		//not sure which one is faster, have to do some test
		/*private function calcDistance2(start:FlxPathfindingNode, end:FlxPathfindingNode):int {
			return Math.abs(n1.x-n2.x)+Math.abs(n1.y-n2.y);
		}*/
		
		

	}
