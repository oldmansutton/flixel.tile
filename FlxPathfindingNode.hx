package flixel.tile;

	
	/**
	* Support structure for the A* pathfinding algorithm
	* @author Vexhel
	*/
	class FlxPathfindingNode {
		public var x:Int;
		public var y:Int;
		public var g:Int = 0;
		public var h:Int = 0;
		public var f:Int = 0;
		public var cost:Int;
		public var movecost:Int;
		public var parent:FlxPathfindingNode = null;
		public var walkable:Bool;

		public function new(x:Int, y:Int, walkable:Bool = true, mcost:Int = 0)	{
			this.x = x;
			this.y = y;
			this.walkable = walkable;
			this.movecost = mcost;
		}
	}
